package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;

    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){

        courseService.createCourse(stringToken, course);
        return new ResponseEntity<>("Course added successfully.", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses(){
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourse(@PathVariable Long courseid, @RequestHeader(value = "Authorization") String stringToken){
        return courseService.deleteCourse(courseid, stringToken);
    }

    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateCourse  (@PathVariable Long courseid, @RequestHeader(value = "Authorization") String stringToken,@RequestBody Course course){
        return courseService.updateCourse(courseid,stringToken, course);
    }

    @RequestMapping(value="/myCourse", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyCourse(@RequestHeader(value="Authorization") String stringToken){
        return new ResponseEntity<>(courseService.getMyCourse(stringToken), HttpStatus.OK);
    }
}
