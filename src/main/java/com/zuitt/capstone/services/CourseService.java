package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void createCourse(String stringToken, Course course);

    Iterable<Course> getCourses();

    ResponseEntity deleteCourse(Long id, String stringToken);

    ResponseEntity updateCourse(Long id, String stringToken, Course course);

    Iterable<Course> getMyCourse(String stringToken);
}

