package com.zuitt.capstone.services;

import com.zuitt.capstone.config.JwtToken;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseRepository;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createCourse(String stringToken, Course course){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    public Iterable<Course> getCourses(){

        return courseRepository.findAll();
    }

    public ResponseEntity updateCourse(Long id, String stringToken, Course course){
        Course courseForUpdate = courseRepository.findById(id).get();
        String courseAuthor = courseForUpdate.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUserName.equals(courseAuthor)){
            courseForUpdate.setName(course.getName());
            courseForUpdate.setDescription(course.getDescription());
            courseForUpdate.setPrice(course.getPrice());
            courseRepository.save(courseForUpdate);

            return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }

    }

    public ResponseEntity deleteCourse(Long id, String stringToken){

        Course courseForUpdating = courseRepository.findById(id).get();
        String courseAuthor = courseForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(courseAuthor)){
            return new ResponseEntity<>("Course deleted successfully", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public Iterable<Course> getMyCourse(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourses();
    }

}
